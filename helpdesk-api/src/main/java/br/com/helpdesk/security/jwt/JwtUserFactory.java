package br.com.helpdesk.security.jwt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import br.com.helpdesk.entity.User;
import br.com.helpdesk.enums.ProfileEnum;

public class JwtUserFactory { // Classe para converter o usuario para o usuario do Spring Security

	public static JwtUser create(User user) { // Crio um JwtUser com base nos dados de um usuario
		return new JwtUser(user.getId(), user.getEmail(), user.getPassword(), mapToGrantedAuthorities(user.getProfile()));
	}

	private static List<GrantedAuthority> mapToGrantedAuthorities(ProfileEnum profileEnum) { // Converto o perfil do usuario para o formato utilizado pelo Spring Security  
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(profileEnum.toString()));

		return authorities;
	}

}
