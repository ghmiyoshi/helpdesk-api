package br.com.helpdesk.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.helpdesk.entity.ChangeStatus;

public interface ChangeStatusRepository extends MongoRepository<ChangeStatus, String> {

	List<ChangeStatus> findByTicketIdOrderByDateChangeStatusDesc(String id);

}
