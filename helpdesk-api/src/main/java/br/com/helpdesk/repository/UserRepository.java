package br.com.helpdesk.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.helpdesk.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmail(String email);

	Page<User> findAll(Pageable paginacao);

}
